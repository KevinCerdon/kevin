@extends('index')
@section('content')
<section class="row" style="margin: 0; height: 85vh;">
    <section class="col-sm-1"></section>
    <section class="col-sm-10">
    <br><h4> Movies: Best Top Movies</h4>
        <table class="table" style="border-style: solid">
            <thead>
                <tr class="table-head" style="background-color: gray; border-style: solid">
                    <th>Movie ID</th>
                    <th>Movie Title</th>
                    <th>Year Made</th>
                    <th>Length</th>
                    <th>Language</th>
                    <th>Date of Release</th>
                    <th>Country Release</th>
                    <th>Movide Details</th>
                </tr>
            </thead>
            <tbody>
            @foreach($allMovies as $movie)
                <tr>
                    <td>{{ $movie->mov_id }}</td>
                    <td>{{ $movie->mov_title }}</td>
                    <td>{{ $movie->mov_year }}</td>
                    <td>{{ $movie->mov_time }}</td>
                    <td>{{ $movie->mov_lang }}</td>
                    <td>{{ $movie->mov_dt_rel }}</td>
                    <td>{{ $movie->mov_rel_country }}</td>
                    <td><a href="{{ url('/movie/movieDetails/'.$movie->mov_id) }}" class="btn btn-secondary">Movie Details</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>

</section>
    <section class="row" style="margin:0;">
         <section class="col-sm-11" style="display:flex; justify-content:center;">
              {{ $allMovies->links() }}
         </section>
</section>
@endsection


