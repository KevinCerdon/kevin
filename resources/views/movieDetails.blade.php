@extends('index')
@section('content')
<section class="row" style="margin: 0; display:flex; justify-content:center;  height: 57vh; padding-top: 25px;">
    <section class="col-sm-1"></section>
    <section class="col-sm-4" style="border-style: solid">
  
  
    <br><h4>Movie Information</h4>


        <table class="table">
            <tr>
                <td>Movie Title: </td>
                    <th>{{$movie->mov_title}}</th>
            </tr>
            <tr>
                <td>Year: </td>
                <td>{{$movie->mov_year}}</td>
            </tr>
            <tr>
                <td>Running Time: </td>
                <td>{{$movie->mov_time}} minutes</td>
            </tr>
            <tr>
                <td>Directed By: </td>
                <td>
                    @foreach ($movie->directors as $director)
                        {{ $director->dir_fname }} {{ $director->dir_lname }}
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>Starring: </td>
                <td>
                    @foreach ($movie->actors as $actor)
                        {{ $actor->act_fname }} {{ $actor->act_lname }}
                    @endforeach 
                     - 
                    @foreach ($movie->actors as $role)
                        {{ $role->pivot->role }}
                    @endforeach 
                </td>
            </tr>
            <tr>
                <td>Genre: </td>
                <td>
                    @foreach ($movie->genres as $genre)
                        {{ $genre->gen_title }}
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>Rating: </td>
                <td>
                    @foreach ($movie->reviewers as $reviewer)
                        {{ $reviewer->rev_name }}
                    @endforeach
                </td>
            </tr>
            <tr>
            <td>Score: </td>
                <td>
                    @foreach ($movie->reviewers as $star)
                        {{ $star->pivot->rev_stars }}
                    @endforeach stars
                </td>
            </tr>
        </table>
    </section>
   
</section>
<section class="row"  style="margin: 0; display:flex; justify-content:center; padding-top: 5px;">
    <section class="col-sm-5" style="margin: 0; display:flex; justify-content:center; padding-top: 5px;">
    <a href="{{ url('/movie/list') }}" class="btn btn-primary">Back to Movie List</a>
</section>
</section>
@endsection
