<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    
    public $table = "actor";
    protected $primaryKey = 'act_id';
    public $incrementing = false;
    public $timestamps = false;

    public function movies(){
        return  $this->belongsToMany('App\Movie', 'movie_cast', 'act_id,', 'mov_id');
    }
    
}
