<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    
    public $table = "genres";
    protected $primaryKey = 'gen_id';
    public $incrementing = false;
    public $timestamps = false;

    public function movieGenres(){
        return $this->belongsToMany('App\Movie','movie_genres', 'gen_id', 'mov_id');
    }
}
