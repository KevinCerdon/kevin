<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
class MovieController extends Controller
{
    //
    public function list(){
       $allMovies = Movie::getAllMovies();
 
       return view('movieAll',compact('allMovies'));
    }

    public function movieDetails(Request $request){
        $movie = Movie::getMovieDetails($request->mov_id);
     
        return view('movieDetails',compact('movie'));

    }
}
