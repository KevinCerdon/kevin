<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    
    public $table = "director";
    protected $primaryKey = 'dir_id';
    public $incrementing = false;
    public $timestamps = false;

    public function movieDirections(){
        return $this->belongsToMany('App\Movie', 'movie_direction', 'dir_id', 'mov_id');
    }

}
