<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    
    public $table = "reviewer";
    protected $primaryKey = 'rev_id';
    public $incrementing = false;
    public $timestamps = false;

    public function rating(){
        return $this->belongsToMany('App\Movie', 'rating', 'rev_id', 'mov_id');
    }
}
