<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(ActorSeeder::class);
        $this->call(DirectorSeeder::class);
        $this->call(MovieSeeder::class);
        $this->call(ReviewerSeeder::class);
        $this->call(GenresSeeder::class);
        $this->call(MovieGenresSeeder::class);
        $this->call(MovieCastSeeder::class);
        $this->call(MovieDirectonSeeder::class);
        $this->call(RatingSeeder::class);

    }
}
